// console.log("Hello World");

const num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave NW","California 9011"];
console.log(`I live at ${address[0]}, ${address[1]} `);

const animal = {
	name: "Lolong",
	type: "Crocodile",
	weight: "1075 kg",
	measurement:"20 ft 3 in"
	
};
const{name,type,weight,measurement} = animal;
console.log(`${name} was a saltwater ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);

let numbers= [1, 2, 3, 4, 5];
numbers.forEach(function(number){
console.log(`${number}`);
});


let sum = 0; 
for (let i = 0; i < numbers.length; i++){ 
	sum += numbers[i]; 
}
console.log(sum); 

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);